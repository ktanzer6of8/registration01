import { useEffect, useState } from 'react';
import _ from "lodash";
import PhoneInput from "react-phone-number-input/input";
import 'react-phone-number-input/style.css';
import useSelector from "react-redux";

export default function Form() {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [npi, setNpi] = useState('');
  const [address, setAddress] = useState('');
  const [telephone, setTelephone] = useState('');

  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    console.log("change in submitted=", submitted);
  }, [submitted]);

  const handleFirstName = (e) => {
    setFirstName(e.target.value);
    setSubmitted(false);
  };
  const handleLastName = (e) => {
    setLastName(e.target.value);
    setSubmitted(false);
  };

  const handleEmail = (e) => {
    setEmail(e.target.value);
    setSubmitted(false);
  };

  const handleAddress = (e) => {
    setAddress(e.target.value);
    setSubmitted(false);
  };

  const handleNpi = (e) => {
    setNpi(e.target.value);
    setSubmitted(false);
  };

  const handleTelephone = (e) => {
    setTelephone(e.target?.value);
    setSubmitted(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if ( _.isEmpty(firstName) || _.isEmpty(lastName) || _.isEmpty(address) || _.isEmpty(email) || _.isEmpty(npi) || _.isEmpty( telephone) ) {
      setError(true);
    } else {
      setSubmitted(true);
      setError(false);
    }
  };

  const successMessage = () => {
    return (
      <div
        className="success"
        style={{
          display: submitted ? '' : 'none',
        }}>
        <h1>User {firstName} {lastName} successfully registered!!</h1>
      </div>
    );
  };

  const errorMessage = () => {
    return (
      <div
        className="error"
        style={{
          display: error ? '' : 'none',
        }}>
        <h1>Please enter all the fields</h1>
      </div>
    );
  };

  return (
    <div className="form">
      <div>
        <h1>User Registration</h1>
      </div>

      {/* Calling to the methods */}
      <div className="messages">
        {errorMessage()}
        {successMessage()}
      </div>

      <form>
        {/*
        First Name
        Last Name
        NPI number
        Business Address
        Telephone Number
        Email address
        */}
        {/* Labels and inputs for form data */}
        <label className="label">First Name</label>
        <input onChange={handleFirstName} className="input"
               value={firstName} type="text" />

        <label className="label">Last Name</label>
        <input onChange={handleLastName} className="input"
               value={lastName} type="text" />

        <label className="label">National Provider Id</label>
        <input onChange={handleNpi} className="input"
               value={npi} type="text" />

        <label className="label">Business Address</label>
        <input onChange={handleAddress} className="input"
               value={address} type="email" />

        <label className="label">Telephone</label>
        {/*<PhoneInput onChange={handleTelephone} style={...} value={telephone} country={"US"}/>*/}
        <input onChange={handleTelephone} className="input"
               value={telephone} type="phone" />

        <label className="label">Email</label>
        <input onChange={handleEmail} className="input"
               value={email} type="email" />

        <button onClick={handleSubmit} className="btn" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
}
